﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcSkiResort.Models;

namespace MvcSkiResort.Controllers
{
    public class ResortController : Controller
    {
        private ResortDBContext db = new ResortDBContext();

        //
        // GET: /Resort/

        public ActionResult Index()
        {
            return View(db.Resort.ToList());
        }

        //
        // GET: /Resort/Details/5

        public ActionResult Details(int id = 0)
        {
            Resort resort = db.Resort.Find(id);
            if (resort == null)
            {
                return HttpNotFound();
            }
            return View(resort);
        }

        //
        // GET: /Resort/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Resort/Create

        [HttpPost]
        public ActionResult Create(Resort resort)
        {
            if (ModelState.IsValid)
            {
                db.Resort.Add(resort);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(resort);
        }

        //
        // GET: /Resort/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Resort resort = db.Resort.Find(id);
            if (resort == null)
            {
                return HttpNotFound();
            }
            return View(resort);
        }

        //
        // POST: /Resort/Edit/5

        [HttpPost]
        public ActionResult Edit(Resort resort)
        {
            if (ModelState.IsValid)
            {
                db.Entry(resort).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(resort);
        }

        //
        // GET: /Resort/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Resort resort = db.Resort.Find(id);
            if (resort == null)
            {
                return HttpNotFound();
            }
            return View(resort);
        }

        //
        // POST: /Resort/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Resort resort = db.Resort.Find(id);
            db.Resort.Remove(resort);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}