﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcSkiResort.Models;

namespace MvcSkiResort.Controllers
{
    public class User2Controller : Controller
    {
        private User2DBContext db = new User2DBContext();

        //
        // GET: /User2/

        public ActionResult Index()
        {
            return View(db.User2.ToList());
        }

        //
        // GET: /User2/Details/5

        public ActionResult Details(int id = 0)
        {
            User2 user2 = db.User2.Find(id);
            if (user2 == null)
            {
                return HttpNotFound();
            }
            return View(user2);
        }

        //
        // GET: /User2/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /User2/Create

        [HttpPost]
        public ActionResult Create(User2 user2)
        {
            if (ModelState.IsValid)
            {
                db.User2.Add(user2);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user2);
        }

        //
        // GET: /User2/Edit/5

        public ActionResult Edit(int id = 0)
        {
            User2 user2 = db.User2.Find(id);
            if (user2 == null)
            {
                return HttpNotFound();
            }
            return View(user2);
        }

        //
        // POST: /User2/Edit/5

        [HttpPost]
        public ActionResult Edit(User2 user2)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user2).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user2);
        }

        //
        // GET: /User2/Delete/5

        public ActionResult Delete(int id = 0)
        {
            User2 user2 = db.User2.Find(id);
            if (user2 == null)
            {
                return HttpNotFound();
            }
            return View(user2);
        }

        //
        // POST: /User2/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            User2 user2 = db.User2.Find(id);
            db.User2.Remove(user2);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}