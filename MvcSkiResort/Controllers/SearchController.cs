﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcSkiResort.Models;

namespace MvcSkiResort.Controllers
{
    public class SearchController : Controller
    {
        private User2DBContext dbUser = new User2DBContext();
        private ResortDBContext dbResort = new ResortDBContext();
        //
        // GET: /Search/

        public ActionResult Index()
        {
            return View(dbUser.User2.ToList());
        }

        public ActionResult Search(int id = 0)
        {
            String ResortName = String.Empty;
            String ResortNameEqual = null;

            //Read in the ID and make sure its not null
            User2 user = dbUser.User2.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            String ExpLevel = user.ExpLevel.ToString();
            //N will keep count of the resorts while HighestValue will keep track of the highest trail number 
            int n = 1;
            int HighestValue = 0;
            Resort resort = dbResort.Resort.Find(n);
            do
            {
                switch(ExpLevel)//Switch case based on ExpLevel
                {
                    case "GreenCircle":
                        if (HighestValue < resort.GreenCircle)
                        {
                            HighestValue = resort.GreenCircle;
                            //Also, every time the HighestValue gets overwritten, the name of the new Resort being analyzed is overwritten into ResortName
                            ResortName = resort.Name.ToString();
                        }
                        else if (HighestValue == resort.GreenCircle)
                        {
                            //If any Resort has the same number of trails with that experience level as the first, they are added to ResortNameEqual
                            ResortNameEqual = ResortNameEqual + resort.Name.ToString();
                        }
                        break;
                    case "BlueSquare":
                            if (HighestValue < resort.BlueSquare)
                        {
                            HighestValue = resort.BlueSquare;
                            ResortName = resort.Name.ToString();
                        }
                        else if (HighestValue == resort.BlueSquare)
                        {
                            ResortNameEqual = ResortNameEqual + resort.Name.ToString();
                        
                        }
                        break;
                    case "BlackDiamond":
                        if (HighestValue < resort.BlackDiamond)
                        {
                            HighestValue = resort.BlackDiamond;
                            ResortName = resort.Name.ToString();
                        }
                        else if (HighestValue == resort.BlackDiamond)
                        {
                            ResortNameEqual = ResortNameEqual + resort.Name.ToString();
                        }
                        break;
                    case "TerrainPark":
                            if (HighestValue < resort.TerrainPark)
                        {
                            HighestValue = resort.TerrainPark;
                            ResortName = resort.Name.ToString();
                        }
                        else if (HighestValue == resort.TerrainPark)
                        {
                            ResortNameEqual = ResortNameEqual + resort.Name.ToString();
                        }
                        break;
                    default:
                        HttpNotFound();
                        break;
                }
                //Iterate our Resort ID by 1 to analyze the next resort
                n = (n + 1);
                resort = dbResort.Resort.Find(n);
            } while (resort != null);
            ViewBag.Message1 = "The first Resort with the highest number (" + HighestValue + ") of trails matching your preferred experience level is: " + ResortName;
            //Finish formatting ResortNameEqual if it is not null
            if (ResortNameEqual != null)
            {
                ResortNameEqual = ResortNameEqual + ".";
            }
            ViewBag.Message2 = "The following Resorts have the same number of trails matching your preferred experience level: " + ResortNameEqual;
            return View();
        }

    }
}
