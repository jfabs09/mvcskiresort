﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcSkiResort.Models
{
    public class Resort
    {
        public int ID { get; set; }
        [Required(ErrorMessage="A Name is required")]
        public String Name { get; set; }
        [Required(ErrorMessage="Black Diamond is required")]
        [Range(0,100,ErrorMessage="Black Diamond must be between 0 and 100")]
        public int BlackDiamond { get; set; }
        [Required(ErrorMessage = "Blue Square is required")]
        [Range(0, 100, ErrorMessage = "Blue Square must be between 0 and 100")]
        public int BlueSquare { get; set; }
        [Required(ErrorMessage = "Green Circle is required")]
        [Range(0, 100, ErrorMessage = "Green Circle must be between 0 and 100")]
        public int GreenCircle { get; set; }
        [Required(ErrorMessage = "TerrainPark is required")]
        [Range(0, 100, ErrorMessage = "TerrainPark must be between 0 and 100")]
        public int TerrainPark { get; set; }
    }
    public class ResortDBContext: DbContext
    {
        public DbSet<Resort> Resort { get; set; }
    }
}
