﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcSkiResort.Models
{
    public class User2
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "A Name is required")]
        public string Name { get; set; }
        public ExperienceLevel ExpLevel { get; set; }
    }
    public enum ExperienceLevel
    {
        GreenCircle,
        BlueSquare,
        BlackDiamond,
        TerrainPark
    }
    public class User2DBContext: DbContext
    {
        public DbSet<User2> User2 {get;set;}
    }
}