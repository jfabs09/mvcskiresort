﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcSkiResort.Models
{
    public class User
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "A Name is required")]
        public string Name { get; set; }
        public bool GreenCircle { get; set; }
        public bool BlueSquare { get; set; }
        public bool BlackDiamond { get; set; }
        public bool TerrainPark { get; set; }
    }
    public class UserDBContext : DbContext
    {
        public DbSet<User> User { get; set; }
    }
}